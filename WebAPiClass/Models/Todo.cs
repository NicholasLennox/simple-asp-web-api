﻿using System.ComponentModel.DataAnnotations;

namespace WebAPiClass.Models
{
    public class Todo
    {
        public int Id { get; set; }
        [Required]
        public string Description { get; set; } = string.Empty;
        public bool Completed { get; set; } = false;
    }
}
