﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebAPiClass.Models;

namespace WebAPiClass.Controllers
{
    [ApiController]
    [Route("api/v1/todos")]
    public class TodoController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetAll()
        {
            var todos = new List<Todo>()
            {
                new Todo(){ Id = 1, Description = "Bath the tiger"},
                new Todo(){ Id = 2, Description = "Repair the bath"},
                new Todo(){ Id = 3, Description = "Get life insurance"}
            };
            return Ok(todos);
        }

        [HttpGet] // api/v1/todos/1
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            return Ok(new Todo() { Id = 1, Description = "Bath the tiger" });
        }

        [HttpPost]
        public IActionResult AddPost(Todo todo)
        {
            // send to the services
            return Created("api/v1/todos/" + todo.Id, null);
        }

        [HttpPut] // api/v1/todos/1
        public IActionResult Update(int id, Todo todo) 
        { 
            if(id != todo.Id)
                return BadRequest();
            // Update in db
            return NoContent();
        }
    }
}
